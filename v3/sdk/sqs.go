package sdk

import (
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/er"
)

//TODO: setup batch sending and receiving
func GetQueueUrl(svc sqsiface.SQSAPI, input *sqs.GetQueueUrlInput) string {
	res, err := svc.GetQueueUrl(input)
	if !er.Handle("could not get queue url", err) {
		return ""
	}

	return *res.QueueUrl
}

func SendMessage(svc sqsiface.SQSAPI, input *sqs.SendMessageInput) bool {
	_, err := svc.SendMessage(input)
	return er.Handle("could send message", err)
}

func ReceiveMessage(svc sqsiface.SQSAPI, input *sqs.ReceiveMessageInput) []*sqs.Message {
	res, err := svc.ReceiveMessage(input)
	if !er.Handle("could not receive message", err) {
		return []*sqs.Message{}
	}

	return res.Messages
}

func DeleteMessage(svc sqsiface.SQSAPI, input *sqs.DeleteMessageInput) bool {
	_, err := svc.DeleteMessage(input)
	return er.Handle("could not delete message", err)
}

func SetQueueAttributes(svc sqsiface.SQSAPI, input *sqs.SetQueueAttributesInput) bool {
	_, err := svc.SetQueueAttributes(input)
	return er.Handle("could not set queue attributes", err)
}
