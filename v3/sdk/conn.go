package sdk

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"log"
	"os"
)

var sess *session.Session

func DynamoConnect() dynamodbiface.DynamoDBAPI {
	if !initSess() {
		return nil
	}
	return dynamodb.New(sess)
}

func SQSConnect() sqsiface.SQSAPI {
	if !initSess() {
		return nil
	}
	return sqs.New(sess)
}

func LambdaConnect() lambdaiface.LambdaAPI {
	if !initSess() {
		return nil
	}
	return lambda.New(sess)
}

func initSess() bool {
	//TODO: find better way to test me
	switch os.Getenv("ENVIRONMENT") {
	case "LOCAL":
		local()
	case "DEV":
		def()
	case "PROD":
		def()
	default:
		log.Println("invalid environment configuration")
		return false
	}
	return true
}

func local() {
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Profile: os.Getenv("AWS_PROFILE"),

		Config: aws.Config{
			Region: aws.String("us-west-2"),
		},

		SharedConfigState: session.SharedConfigEnable,
	}))
}

func def() {
	sess = session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{
			MaxRetries: aws.Int(3),
			Region:     aws.String("us-west-2"),
		},
		SharedConfigState: session.SharedConfigEnable,
	}))
}
