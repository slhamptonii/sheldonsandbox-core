package sdk

import (
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestRespond(t *testing.T) {
	headers := map[string]string{"apple": "orange"}
	res, err := Respond(http.StatusOK, headers, "okie dokie", errors.New("test err"))
	assert.Equal(t, 200, res.StatusCode)
	assert.Equal(t, headers, res.Headers)
	assert.Equal(t, "okie dokie", res.Body)
	assert.Equal(t, err.Error(), "test err")
}

func TestShortRes(t *testing.T) {
	headers := map[string]string{"apple": "orange"}
	res, err := ShortRes(http.StatusOK, headers, "okie dokie")
	assert.Equal(t, 200, res.StatusCode)
	assert.Equal(t, headers, res.Headers)
	assert.Equal(t, "okie dokie", res.Body)
	assert.Nil(t, err)
}

func TestNoAuthRes(t *testing.T) {
	res, err := NoAuthRes(http.StatusOK, "okie dokie")
	assert.Equal(t, 200, res.StatusCode)
	assert.Nil(t, res.Headers)
	assert.Equal(t, "okie dokie", res.Body)
	assert.Nil(t, err)
}

func TestPullFromCtx(t *testing.T) {
	ctx := events.APIGatewayProxyRequestContext{Authorizer: map[string]interface{}{
		"apple":    "banana",
		"broccoli": "cider",
		"left":     "right",
	}}

	out := PullFromCtx(&ctx, "apple", "left")
	assert.Len(t, out, 2)
	assert.Equal(t, "banana", out[0])
	assert.Equal(t, "right", out[1])
}

func TestInvoke(t *testing.T) {
	m := mockLambdaOutItem{Err: nil}
	res, ok := Invoke(&m, &lambda.InvokeInput{})
	assert.True(t, ok)
	assert.Equal(t, "test log", *res.LogResult)
}

type mockLambdaOutItem struct {
	lambdaiface.LambdaAPI
	Err error
}

func (m mockLambdaOutItem) Invoke(input *lambda.InvokeInput) (*lambda.InvokeOutput, error) {
	return &lambda.InvokeOutput{LogResult: aws.String("test log")}, m.Err
}
