package sdk

import (
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDynamoConnect(t *testing.T) {
	svc := DynamoConnect()
	_, ok := svc.(dynamodbiface.DynamoDBAPI)
	assert.True(t, ok)
}

func TestSQSConnect(t *testing.T) {
	svc := SQSConnect()
	_, ok := svc.(sqsiface.SQSAPI)
	assert.True(t, ok)
}

func TestLambdaConnect(t *testing.T) {
	svc := LambdaConnect()
	_, ok := svc.(lambdaiface.LambdaAPI)
	assert.True(t, ok)
}

func TestInitSess(t *testing.T) {
	assert.True(t, initSess())
}

func TestLocal(t *testing.T) {
	local()
	assert.Equal(t, "us-west-2", *sess.Config.Region)
}

func TestProd(t *testing.T) {
	def()
	assert.Equal(t, "us-west-2", *sess.Config.Region)
}
