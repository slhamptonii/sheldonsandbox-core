package sdk

import (
	"errors"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetQueueUrl(t *testing.T) {
	m := mockSQSOutItem{}
	url := GetQueueUrl(&m, &sqs.GetQueueUrlInput{})
	assert.Equal(t, "url", url)

	m.Err = errors.New("test err")
	url = GetQueueUrl(&m, &sqs.GetQueueUrlInput{})
	assert.Empty(t, url)
}

func TestSendMessage(t *testing.T) {
	m := mockSQSOutItem{}
	ok := SendMessage(&m, &sqs.SendMessageInput{})
	assert.True(t, ok)
}

func TestReceiveMessage(t *testing.T) {
	m := mockSQSOutItem{}
	messages := ReceiveMessage(&m, &sqs.ReceiveMessageInput{})
	assert.Len(t, messages, 1)
	assert.Equal(t, "id", *messages[0].MessageId)

	m.Err = errors.New("test err")
	messages = ReceiveMessage(&m, &sqs.ReceiveMessageInput{})
	assert.Len(t, messages, 0)
}

func TestDeleteMessage(t *testing.T) {
	m := mockSQSOutItem{}
	ok := DeleteMessage(&m, &sqs.DeleteMessageInput{})
	assert.True(t, ok)
}

func TestSetQueueAttributes(t *testing.T) {
	m := mockSQSOutItem{}
	ok := SetQueueAttributes(&m, &sqs.SetQueueAttributesInput{})
	assert.True(t, ok)
}

type mockSQSOutItem struct {
	sqsiface.SQSAPI
	Err error
}

func (m *mockSQSOutItem) GetQueueUrl(input *sqs.GetQueueUrlInput) (*sqs.GetQueueUrlOutput, error) {
	url := "url"
	return &sqs.GetQueueUrlOutput{QueueUrl: &url}, m.Err
}

func (m *mockSQSOutItem) SendMessage(input *sqs.SendMessageInput) (*sqs.SendMessageOutput, error) {
	return &sqs.SendMessageOutput{}, m.Err
}

func (m *mockSQSOutItem) ReceiveMessage(input *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	id := "id"
	return &sqs.ReceiveMessageOutput{Messages: []*sqs.Message{{MessageId: &id}}}, m.Err
}

func (m *mockSQSOutItem) DeleteMessage(input *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	return &sqs.DeleteMessageOutput{}, m.Err
}

func (m *mockSQSOutItem) SetQueueAttributes(input *sqs.SetQueueAttributesInput) (*sqs.SetQueueAttributesOutput, error) {
	return &sqs.SetQueueAttributesOutput{}, m.Err
}
