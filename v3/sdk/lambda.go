package sdk

import (
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/lambda/lambdaiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/er"
)

func NoAuthRes(code int, msg string) (events.APIGatewayProxyResponse, error) {
	return Respond(code, nil, msg, nil)
}

func ShortRes(code int, headers map[string]string, msg string) (events.APIGatewayProxyResponse, error) {
	return Respond(code, headers, msg, nil)
}

func Respond(code int, headers map[string]string, msg string, e error) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: code,
		Headers:    headers,
		Body:       msg,
	}, e
}

func PullFromCtx(ctx *events.APIGatewayProxyRequestContext, args ...string) []interface{} {
	out := make([]interface{}, len(args))

	for i, val := range args {
		out[i] = ctx.Authorizer[val]
	}

	return out
}

func Invoke(svc lambdaiface.LambdaAPI, input *lambda.InvokeInput) (*lambda.InvokeOutput, bool) {
	res, err := svc.Invoke(input)
	msg := fmt.Sprintf("could not invoke lambda %s ", input.String())
	return res, er.Handle(msg, err)
}
