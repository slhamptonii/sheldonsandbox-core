package sdk

import (
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/model"
	"testing"
)

func TestPut(t *testing.T) {
	//Success
	m := &mockDynamoOutItem{}
	ok := Put(m, "table", model.User{})
	assert.Equal(t, true, ok)

	//Fail
	m = &mockDynamoOutItem{Err: errors.New("test error")}
	ok = Put(m, "table", model.User{})
	assert.Equal(t, false, ok)
}

func TestPutItem(t *testing.T) {
	m := mockDynamoOutItem{Err: nil}
	ok := PutItem(&m, &dynamodb.PutItemInput{})
	assert.Equal(t, true, ok)
}

func TestGetItem(t *testing.T) {
	m := mockDynamoOutItem{Err: nil}
	res, ok := GetItem(&m, &dynamodb.GetItemInput{})
	assert.True(t, ok)
	assert.Equal(t, "test table", *res.ConsumedCapacity.TableName)
}

func TestUpdateItem(t *testing.T) {
	m := mockDynamoOutItem{Err: nil}
	res, ok := UpdateItem(&m, &dynamodb.UpdateItemInput{})
	assert.True(t, ok)
	assert.Equal(t, "test table", *res.ConsumedCapacity.TableName)
}

func TestDeleteItem(t *testing.T) {
	m := mockDynamoOutItem{Err: nil}
	ok := DeleteItem(&m, &dynamodb.DeleteItemInput{})
	assert.Equal(t, true, ok)
}

type mockDynamoOutItem struct {
	dynamodbiface.DynamoDBAPI
	Err error
}

func (m *mockDynamoOutItem) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	return &dynamodb.PutItemOutput{}, m.Err
}

func (m mockDynamoOutItem) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	return &dynamodb.GetItemOutput{ConsumedCapacity: &dynamodb.ConsumedCapacity{TableName: aws.String("test table")}}, m.Err
}

func (m mockDynamoOutItem) UpdateItem(input *dynamodb.UpdateItemInput) (*dynamodb.UpdateItemOutput, error) {
	return &dynamodb.UpdateItemOutput{ConsumedCapacity: &dynamodb.ConsumedCapacity{TableName: aws.String("test table")}}, m.Err
}

func (m mockDynamoOutItem) DeleteItem(input *dynamodb.DeleteItemInput) (*dynamodb.DeleteItemOutput, error) {
	return &dynamodb.DeleteItemOutput{}, m.Err
}
