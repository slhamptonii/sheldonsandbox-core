package sdk

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbiface"
	"gitlab.com/slhamptonii/sheldonsandbox-core/v3/er"
	"log"
)

func Put(svc dynamodbiface.DynamoDBAPI, table string, i interface{}) bool {
	av, err := dynamodbattribute.MarshalMap(i)
	if err != nil {
		log.Println("could not marshal interface", err)
		return false
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String(table),
	}

	return PutItem(svc, input)
}

func PutItem(svc dynamodbiface.DynamoDBAPI, input *dynamodb.PutItemInput) bool {
	_, err := svc.PutItem(input)
	msg := fmt.Sprintf("could not put input %s ", input.String())
	return er.Handle(msg, err)
}

func GetItem(svc dynamodbiface.DynamoDBAPI, input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, bool) {
	res, err := svc.GetItem(input)
	msg := fmt.Sprintf("could not get input %s ", input.String())
	return res, er.Handle(msg, err)
}

func UpdateItem(svc dynamodbiface.DynamoDBAPI, input *dynamodb.UpdateItemInput) (*dynamodb.UpdateItemOutput, bool) {
	res, err := svc.UpdateItem(input)
	return res, er.HandleDynamodbErr(err)
}

func DeleteItem(svc dynamodbiface.DynamoDBAPI, input *dynamodb.DeleteItemInput) bool {
	_, err := svc.DeleteItem(input)
	msg := fmt.Sprintf("could not delete input %s ", input.String())
	return er.Handle(msg, err)
}
