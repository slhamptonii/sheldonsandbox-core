package er

import (
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestHandle(t *testing.T) {
	assert.Equal(t, true, Handle("op", nil))
	assert.Equal(t, false, Handle("op", errors.New("test err")))
}
