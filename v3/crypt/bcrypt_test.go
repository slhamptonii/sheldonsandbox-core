package crypt

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestComparePasswords(t *testing.T) {
	assert.True(t, ComparePasswords("$2a$04$TqXBBTTnZbjpOND/HkTWyOmFvFxqOuXSrafSWiGj7V4RN3qdDxo3O", []byte("drowssap")))
	assert.False(t, ComparePasswords("jibberish", []byte("drowssap")))
}

func TestHashAndSaltMin(t *testing.T) {
	assert.True(t, HashAndSaltMin([]byte("apple")) != "")
	assert.True(t, HashAndSaltDefault([]byte("apple")) != "")
	//test runs all sorts of slowly
	//assert.True(t, HashAndSaltMax([]byte("apple")) != "")
}
