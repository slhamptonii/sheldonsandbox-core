package request

import "strings"

func ExtractPath(path string) []string {
	split := strings.Split(path, "/")
	output := make([]string, 0)

	for _, c := range split {
		if c != "" {
			output = append(output, strings.Trim(c, "\r\n "))
		}
	}

	return output
}
