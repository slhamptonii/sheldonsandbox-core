package request

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestExtractPath(t *testing.T) {
	out := ExtractPath("/users/blah/items/rubber-duck")
	assert.Equal(t, 4, len(out))
	assert.Equal(t, "users", out[0])
	assert.Equal(t, "blah", out[1])
	assert.Equal(t, "items", out[2])
	assert.Equal(t, "rubber-duck", out[3])
}
