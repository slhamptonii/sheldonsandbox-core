package request

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

type HttpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

type Requester struct {
	ServiceURL string
	Client     HttpClient
}

func NewRequester(host string) Requester {
	return Requester{
		ServiceURL: host,
		Client: &http.Client{
			Timeout: 20 * time.Second,
			Transport: &http.Transport{
				MaxConnsPerHost:     10,
				MaxIdleConnsPerHost: 5,
			},
		}}
}

func (r *Requester) Head(path string, headers map[string]string) (*http.Response, error) {
	return r.request("HEAD", path, headers, nil)
}

func (r *Requester) Post(path string, headers map[string]string, data interface{}) (*http.Response, error) {
	return r.request("POST", path, headers, data)
}

func (r *Requester) Get(path string, headers map[string]string) (*http.Response, error) {
	return r.request("GET", path, headers, nil)
}

func (r *Requester) Patch(path string, headers map[string]string, data interface{}) (*http.Response, error) {
	return r.request("PATCH", path, headers, data)
}

func (r *Requester) Put(path string, headers map[string]string, data interface{}) (*http.Response, error) {
	return r.request("PUT", path, headers, data)
}

func (r *Requester) Delete(path string, headers map[string]string) (*http.Response, error) {
	return r.request("DELETE", path, headers, nil)
}

func (r *Requester) request(method string, path string, headers map[string]string, data interface{}) (*http.Response, error) {
	payload, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	reader := bytes.NewBuffer(payload)
	req, err := http.NewRequest(method, r.ServiceURL+path, reader)
	if err != nil {
		return nil, err
	}

	for k, v := range headers {
		//req.Set canonicalizes the header keys
		// To use non-canonical keys, assign to the map directly.
		req.Header[k] = []string{v}
	}

	return r.Client.Do(req)
}
