package request

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestNewRequester(t *testing.T) {
	r := NewRequester("host")
	assert.Equal(t, "host", r.ServiceURL)
}

func TestRequester_Head(t *testing.T) {
	assert := assert.New(t)
	req := NewRequester("host")
	req.Client = &ClientMock{
		Eval: func(r *http.Request) {
			assert.Equal("HEAD", r.Method)
			assert.Equal(req.ServiceURL+"path", r.URL.Path)
			assert.Len(r.Header, 0)
		},
	}

	_, _ = req.Head("path", map[string]string{})
}

func TestRequester_Post(t *testing.T) {
	assert := assert.New(t)
	req := NewRequester("host")
	req.Client = &ClientMock{
		Eval: func(r *http.Request) {
			assert.Equal("POST", r.Method)
			assert.Equal(req.ServiceURL+"path", r.URL.Path)
			assert.Len(r.Header, 0)
		},
	}

	_, _ = req.Post("path", map[string]string{}, "body")
}

func TestRequester_Get(t *testing.T) {
	assert := assert.New(t)
	req := NewRequester("host")
	req.Client = &ClientMock{
		Eval: func(r *http.Request) {
			assert.Equal("GET", r.Method)
			assert.Equal(req.ServiceURL+"path", r.URL.Path)
			assert.Len(r.Header, 0)
		},
	}

	_, _ = req.Get("path", map[string]string{})
}

func TestRequester_Patch(t *testing.T) {
	assert := assert.New(t)
	req := NewRequester("host")
	req.Client = &ClientMock{
		Eval: func(r *http.Request) {
			assert.Equal("PATCH", r.Method)
			assert.Equal(req.ServiceURL+"path", r.URL.Path)
			assert.Len(r.Header, 0)
		},
	}

	_, _ = req.Patch("path", map[string]string{}, nil)
}

func TestRequester_Put(t *testing.T) {
	assert := assert.New(t)
	req := NewRequester("host")
	req.Client = &ClientMock{
		Eval: func(r *http.Request) {
			assert.Equal("PUT", r.Method)
			assert.Equal(req.ServiceURL+"path", r.URL.Path)
			assert.Len(r.Header, 0)
		},
	}

	_, _ = req.Put("path", map[string]string{}, nil)
}

func TestRequester_Delete(t *testing.T) {
	assert := assert.New(t)
	req := NewRequester("host")
	req.Client = &ClientMock{
		Eval: func(r *http.Request) {
			assert.Equal("DELETE", r.Method)
			assert.Equal(req.ServiceURL+"path", r.URL.Path)
			assert.Len(r.Header, 0)
		},
	}

	_, _ = req.Delete("path", map[string]string{})
}

func TestRequest(t *testing.T) {
	assert := assert.New(t)
	//Success
	req := NewRequester("host")
	req.Client = &ClientMock{
		Eval: func(r *http.Request) {
			assert.Equal("OPTIONS", r.Method)
			assert.Equal(req.ServiceURL+"path", r.URL.Path)
			assert.Len(r.Header, 1)
		},
	}
	_, _ = req.request("OPTIONS", "path", map[string]string{"alfa": "bravo"}, nil)
}

type ClientMock struct {
	Eval func(r *http.Request)
}

func (c *ClientMock) Do(req *http.Request) (*http.Response, error) {
	c.Eval(req)
	return &http.Response{}, nil
}
