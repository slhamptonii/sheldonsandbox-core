module gitlab.com/slhamptonii/sheldonsandbox-core/v3

go 1.14

require (
	github.com/aws/aws-lambda-go v1.21.0
	github.com/aws/aws-sdk-go v1.36.15
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
)
