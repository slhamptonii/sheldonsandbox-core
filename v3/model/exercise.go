package model

type Exercise struct {
	Name      string   `db:"name" json:"name,omitempty" `
	Muscles   []Muscle `db:"muscles" json:"muscles,omitempty" `
	Sets      []Set    `db:"sets" json:"sets,omitempty" `
	SuperSets []string `db:"superSets" json:"superSets,omitempty" `
}

type Exercises []Exercise
