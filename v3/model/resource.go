package model

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
)

func (e Exercises) Value() (driver.Value, error) {
	j, err := json.Marshal(e)
	return j, err
}

func (e *Exercises) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("type assertion .([]byte) failed")
	}

	err := json.Unmarshal(source, &e)
	if err != nil {
		return err
	}

	return nil
}

func (m UserMeta) Value() (driver.Value, error) {
	j, err := json.Marshal(m)
	return j, err
}

func (m *UserMeta) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("type assertion .([]byte) failed")
	}

	err := json.Unmarshal(source, &m)
	if err != nil {
		return err
	}

	return nil
}

func (m DraftMeta) Value() (driver.Value, error) {
	j, err := json.Marshal(m)
	return j, err
}

func (m *DraftMeta) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("type assertion .([]byte) failed")
	}

	err := json.Unmarshal(source, &m)
	if err != nil {
		return err
	}

	return nil
}

func (p Party) Value() (driver.Value, error) {
	j, err := json.Marshal(p)
	return j, err
}

func (p *Party) Scan(src interface{}) error {
	source, ok := src.([]byte)
	if !ok {
		return errors.New("type assertion .([]byte) failed")
	}

	err := json.Unmarshal(source, &p)
	if err != nil {
		return err
	}

	return nil
}
