package model

type Pick struct {
	Turn     int    `json:"turn" db:"turn"`
	Username string `json:"username" db:"username"`
	ItemName string `json:"itemName" db:"itemName"`
}

type Draft struct {
	Id      int       `json:"id,omitempty" validate:"min=0"`
	Party   int    `json:"party,omitempty" validate:"min=0"`
	Running bool      `json:"state"`
	Turn    int       `json:"turn"`
	Meta    DraftMeta `json:"meta" db:"meta"`
	CreatedDateTime string   `json:"createdDateTime"`
	UpdatedDateTime string   `json:"updatedDateTime"`
}

type DraftMeta struct {
	Items []Item `json:"items" db:"items"`
	Order []int  `json:"order" db:"order"`
	Picks []Pick `json:"picks" db:"picks"`
}
