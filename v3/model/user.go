package model

type Role string

type User struct {
	Id              int      `json:"id,omitempty" validate:"min=0"`
	Username        string   `json:"username,omitempty" validate:"min=4,max=40,required"`
	Password        string   `json:"password,omitempty"`
	CreatedDateTime string   `json:"createdDateTime"`
	UpdatedDateTime string   `json:"updatedDateTime"`
	Roles           []Role   `json:"roles" validate:"required,oneof=Stranger User Admin"`
	Meta            UserMeta `json:"meta,omitempty" db:"meta"`
	Parties         []int `json:"parties,omitempty" db:"parties"`
}

const (
	STRANGER Role = "STRANGER"
	PLAYER   Role = "PLAYER"
	ADMIN    Role = "ADMIN"
)

type UserMeta struct {
	IsActive    bool   `json:"isActive" db:"isActive"`
	IsSuspended bool   `json:"isSuspended" db:"isSuspended"`
	IsVerified  bool   `json:"isVerified" db:"isSuspended"`
	Email       string `json:"email" db:"email"`
}
