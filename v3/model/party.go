package model

type Party struct {
	Id       int      `json:"id,omitempty" validate:"min=0"`
	Name     string   `json:"name"`
	DM       int   `json:"dm"`
	Members  []int `json:"members,omitempty" db:"members" validate:"min=0"`
	Password string   `json:"password,omitempty"`
	CreatedDateTime string   `json:"createdDateTime"`
	UpdatedDateTime string   `json:"updatedDateTime"`
}
