package model

type Item struct {
	Id          int    `json:"id,omitempty" db:"id" validate:"min=0"`
	Name        string `json:"name" db:"name"`
	Description string `json:"description" db:"description"`
	Owner       int `json:"owner,omitempty" db:"owner" validate:"min=0"`
	CreatedDateTime string   `json:"createdDateTime" db:"createdDateTime"`
	UpdatedDateTime string   `json:"updatedDateTime" db:"createdDateTime"`
}
